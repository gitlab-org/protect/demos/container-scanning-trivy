# container-scanning-trivy

An example of project using Trivy through container scanning.

The image to be scanned is set [here](https://gitlab.com/gitlab-org/threat-management/defend/demos/container-scanning-trivy/-/blob/master/.gitlab-ci.yml#L2).
